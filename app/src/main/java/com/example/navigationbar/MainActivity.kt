package com.example.navigationbar

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.navigationbar.ui.gallery.GalleryFragment
import com.example.navigationbar.ui.home.HomeFragment
import com.example.navigationbar.ui.slideshow.SlideshowFragment
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var adapter:Adapter
    var itemlist= mutableListOf<ItemModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        supportFragmentManager.beginTransaction().add(R.id.nav_host_fragment,HomeFragment(),"Home").commit()
      //  val toolbar: Toolbar = findViewById(R.id.toolbar)
      //  setSupportActionBar(toolbar)


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
//       val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow), drawerLayout)
      // setupActionBarWithNavController(navController, appBarConfiguration)

      //  navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
    private fun init(){
        menuu.setOnClickListener(){
            drawer_layout.openDrawer(GravityCompat.START)
        }
        itemlist.add(ItemModel(R.drawable.ic_menu_camera,"Home"))
        itemlist.add(ItemModel(R.drawable.ic_menu_gallery,"Galerry"))
        itemlist.add(ItemModel(R.drawable.ic_menu_slideshow,"Slideshow"))
        adapter= Adapter(itemlist,this,object:Onclick{
            override fun click(position: Int) {

                when(itemlist[position].title){
                    "Home"->openfragment(HomeFragment(),"Home")
                    "Galerry"->openfragment(GalleryFragment(),"Galerry")
                    "Slideshow"->openfragment(SlideshowFragment(),"Slideshow")
                }
             }
        })
        recyclerview.adapter=adapter
        recyclerview.layoutManager=LinearLayoutManager(this)

    }

    fun openfragment( frga:Fragment,tag:String){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.nav_host_fragment,frga,"Home").commit()


    }



}
