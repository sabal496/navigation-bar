package com.example.navigationbar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_layout.view.*


class Adapter(
    val mylist:MutableList<ItemModel>,
    val activity: MainActivity,
    val click: Onclick
   ):RecyclerView.Adapter<Adapter.ViewHolder>() {

    var pos:Int=0
    var selectedItem: Int=0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_layout,parent,false))
    }

    override fun getItemCount()=mylist.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onbind()
    }
    inner class ViewHolder(itemView :View):RecyclerView.ViewHolder(itemView){
        lateinit var model:ItemModel
        fun onbind(){
            model=mylist[adapterPosition]
            itemView.iccon.setImageResource(model.icon)
            itemView.tittle.text=model.title

            itemView.setBackgroundColor(ContextCompat.getColor(activity,android.R.color.white))
            if (selectedItem == adapterPosition) {
                itemView.setBackgroundColor(ContextCompat.getColor(activity,android.R.color.darker_gray))
            }

            itemView.setOnClickListener(){
                click.click(adapterPosition)
                selectedItem = adapterPosition
                notifyDataSetChanged()

            }
        }


    }
}